import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Person } from '../class_/person';
import { LocalstorageService } from '../services/localstorage.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  person: Person[] = [];
  public _hasRecords:boolean = true;
  public _isSkeleton:boolean = true;
  mainForm: FormGroup;
  submitAttempt: boolean = false;
  data = [];

  constructor(
    private apiService: ApiService,
    private localStorage:LocalstorageService,
    public formBuilder: FormBuilder
  ) {
      this.mainForm = this.formBuilder.group({
        nombre: ['',Validators.compose([Validators.required])],
        edad: ['',Validators.compose([Validators.required])],
        sexo: ['',Validators.compose([Validators.required])],
        documento: ['',Validators.compose([Validators.required])]
      });
   }

  ngOnInit(): void {

    this.apiService.getRequest().subscribe((data: any[])=>{

      if(data['results'].length > 0)
      {
        //console.log(data['results']);
        this.person = data['results'];
      }
    });

    setTimeout(()=>{
      //this.localStorage.clear();
      this.getLocalPerson();

      this._isSkeleton = false;
      if(!this.person.length) this._hasRecords = false; else  this._hasRecords = true;
    },2000);
  }

  getLocalPerson()
  {
    let person = this.localStorage.get('person');
    if(person != null && person.length > 0)
    {
      for (var i = 0; i < person.length; i++) {

        this.data.push(
          {
            nombre:person[i].nombre,
            edad: person[i].edad,
            sexo:person[i].sexo,
            documento:person[i].documento
          }
        );

        this.person.push(
          {
            nombre:person[i].nombre,
            edad: person[i].edad,
            sexo:person[i].sexo,
            documento:person[i].documento
          }
        );
      }
    }

    console.log("local 1",this.data);

  }

  storeData()
  {
    this.submitAttempt = true;

    if(this.mainForm.valid)
    {

        this.data.push(
          {
            nombre:this.mainForm.value.nombre,
            edad: this.mainForm.value.edad,
            sexo:this.mainForm.value.sexo,
            documento:this.mainForm.value.documento
          }
        );

        this.person.push(
          {
            nombre:this.mainForm.value.nombre,
            edad: this.mainForm.value.edad,
            sexo:this.mainForm.value.sexo,
            documento:this.mainForm.value.documento
          }
        );

        console.log(this.data);
        this.localStorage.set('person',this.data);
        this.submitAttempt = false;
        this.mainForm.reset();
    }
  }
}
