import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiURL: string = 'https://randomapi.com/api/9oke07ao?key=E80H-AOHP-I1TY-WP31&results=5';

  constructor(private httpClient: HttpClient) {}

  public getRequest(){
    return this.httpClient.get(this.apiURL);
  }
}
